
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/BIOS.h>
#include <ti/drivers/GPIO.h>

#define PDM_EVT_START                   Event_Id_01
#define PDM_EVT_STOP                    Event_Id_02
#define PDM_EVT_WAIT_DECIMATION         Event_Id_03
#define PDM_EVT_DATA_READY              Event_Id_04
#define PCM_BUF_SIZE             50     /* Samples - Must be at least 32 */

int16_t pcmBufL1[PCM_BUF_SIZE]; /* make it 32-bit later on */
//uint32_t events = 0;
Event_Handle pdmEvents;
Event_Struct sPDMEvents;
Event_Handle birdEvents;
Event_Struct sbirdEvents;
