# bird_detection_on_edge

<b>Description:</b> A real-time ultra-light bioacoustic classifier using template-based supoort vector machine on edge

<b>Requirements for the demo:</b>

- <i>Target board:</i> [TI's CC1352R launchpad](https://www.ti.com/tool/LAUNCHXL-CC1352R1) 
- <i>Microphone: </i> [Adafruit PDM MEMS Microphone.](https://www.adafruit.com/product/3492)
- <i>Software:</i> [Code Composer Studio(CCS)](https://www.ti.com/tool/CCSTUDIO)


<b> Connections: </b> 

| PDM microphone | CC1352R launchpad |
| ------ | ------ |
| 3.3V | 3.3V |
| GND | GND |
| SEL | GND |
| DAT | DIO26 |
| CLK | DIO27 |


<b>Files Description: </b>
        <ul>
                <li><i>main_file.c:</i> bird_detecion function, SVM_CAR inference.</li>
                <li><i>pdmstream.c:</i> PDM microphone interface.</li>
                <li><i>main_tirtos.c:</i> main function with two threads, bird_detection and pdmstream.</li>
        </ul>

<b> Notes: </b> 
- ARM Cotex M4F is being used at it's max frequncy 48MHz.
- Floating point unit(FPU) is used.
- PDMstream info:  
        <ul>
                <li>_Oversampling rate(Clock rate):_ 1MHz</li>
                <li>_PCM Data rate_: 16KHz.</li>
                <li>_Decimation:_ 64</li>
        </ul>
- Memory usage for the current code:
        <ul>
                <li>93 percent of 80KB SRAM is used currently.</li>
                <li>13 percent of 360KB flash is used currently.</li>
        </ul> 

<b> Task list: </b> 
- [ ] _Reduce the execution time of bird_detection:_ Convert the bird_detection calculations from floating point to integers.
- [ ] _Pipleline check:_ Check the whole pipeline output on the CRO whether the processed audio samples are giving right output 
- [ ] _Memory optimizations:_ Need to brushup the code, remove unnecessary declarations and handles. 




