#include<math.h>
#include<stdio.h>
#include"CAR.h"


void filterCoeff(struct filter_coeff coeffPtr[NFILT],float x_low, float x_high,unsigned int fs,unsigned int totalFilters)
{
	int i;
    float x_diff =0;
	float x[totalFilters],frq[totalFilters];
	float damping_factor = 0.1;
	x_diff = ((float)(x_high - x_low))/((float)totalFilters-1);
	float rF1=0;
	for(i = 0; i < totalFilters; i++)
	{	

		x[i] = x_high - i * x_diff;

		frq[i] = 165.4 * (pow(10,(2.1*x[i]))-1);
		coeffPtr[i].a0f = cos(2*M_PI*(frq[i]/fs)); 
		coeffPtr[i].c0f = sin(2*M_PI*(frq[i]/fs));
		coeffPtr[i].rf  = (1-damping_factor*2*M_PI*(frq[i]/fs));
	       	coeffPtr[i].hf  = coeffPtr[i].c0f;
		rF1 = coeffPtr[i].rf * coeffPtr[i].rf;
		coeffPtr[i].gh  = (1- 2*coeffPtr[i].a0f * coeffPtr[i].rf+ rF1) /(1-(2*coeffPtr[i].a0f-coeffPtr[i].hf * coeffPtr[i].c0f)*coeffPtr[i].rf+rF1);
	//	printf("gh[%d] = %10f \n",i,coeffPtr[i].gh);
	}
}
