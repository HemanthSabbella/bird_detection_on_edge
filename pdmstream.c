/*
 * Copyright (c) 2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== pdmstream.c ========
 */

/* Select the type of decimation filter to use
 *   Comment the following line to use TI decimation filter
 *   Uncomment the following line to use custom decimation filter */
//#define USE_CUSTOM_FILTER

/* Select the type of data to send over the UART
 *   Comment the following line to send out regular PCM data
 *   Uncomment the following line to send out ADPCM data */
//#define SEND_ADPCM_DATA

#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <unistd.h>

/* Driver Header files */

#include <ti/drivers/PDM.h>
#include <ti/drivers/PIN.h>
#include "pdmstream.h"
//#include <ti/sysbios/knl/Event.h>
//#include <ti/sysbios/BIOS.h>
#include <ti/drivers/UART.h>

/* Codec and filters Header files */
#ifdef SEND_ADPCM_DATA
#include <ti/adpcm/Codec1.h>
#endif

#ifdef USE_CUSTOM_FILTER
#include <ti/drivers/pdm/PDMFilter/PDMFilterCustom.h>
#define PDM_FREQUENCY            (48000000/25) /*Hz - When using CC26XX, PDM frequency must be an integer divider of 48MHz*/
#define DECIMATION_FACTOR        120           /* Must match your custom filter - Must be set to 64 or 128. */
#else
#include <ti/drivers/pdm/PDMFilter/PDMFilterTI.h>
#define PDM_FREQUENCY            (48000000/47) /*Hz - When using CC26XX, PDM frequency must be an integer divider of 48MHz*/
#define DECIMATION_FACTOR        64      /* Must match your custom filter - Must be set to 64 or 128. */
#endif

/* 128 and 2 mega hertz */


/* Example/Board Header files */
#include "Board.h"

/*
 * PDM settings
 */
#define NUMBER_OF_DATA_LINES     1       /* Must be set to 1 with custom filter - Can be set to 1 or 2 with TI filter. */
//#define PCM_BUF_SIZE             32      /* Samples - Must be at least 32 */
#define PDM_BUF_SIZE             (getPdmBufferSize(PCM_BUF_SIZE, DECIMATION_FACTOR, NUMBER_OF_DATA_LINES))
#define NUMBER_OF_TRANSACTIONS   6

/*
 * UART settings
 */
//#define UART_BAUD_RATE            460800
//#define UART_HEADER_SIZE          4

/*
 * Handles used
 */
static PDM_Handle microphonePdmHandle;

/*
 * Application LED pin configuration table:
 *   - All LEDs board LEDs are off.
 */
PIN_Config ledPinTable[] = {
    Board_GPIO_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    Board_GPIO_LED0 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

/*
 * Application button pin configuration table:
 *   - Buttons interrupts are configured to trigger on falling edge.
 *   - If the UP, DOWN, SELECT inputs are defined add them to the table.
 */
PIN_Config buttonPinTable[] = {
    Board_GPIO_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    Board_GPIO_BUTTON1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};

static bool transferIsRunning = (bool)false;

//static uint8_t sequenceNumber = 0;
//uint8_t uartBuff[PCM_BUF_SIZE*sizeof(uint16_t) + UART_HEADER_SIZE];

/*
 * Application PDM and PCM buffers
 */
int8_t pdmBuf1[PDM_BUF_SIZE];
int8_t pdmBuf2[PDM_BUF_SIZE];
int8_t pdmBuf3[PDM_BUF_SIZE];
int8_t pdmBuf4[PDM_BUF_SIZE];
int8_t pdmBuf5[PDM_BUF_SIZE];
int8_t pdmBuf6[PDM_BUF_SIZE];

//int16_t pcmBufL1[PCM_BUF_SIZE]; /* make it 32-bit later on */

#ifdef SEND_ADPCM_DATA
int8_t  si = 0;
int16_t pv = 0;
uint8_t adpcmBuf[PCM_BUF_SIZE/2 + UART_HEADER_SIZE]; /* Each PCM sample is coded by 4 bits */
#endif

#ifndef USE_CUSTOM_FILTER
/* TI filters require the application to keep track of the filter state */
uint32_t decimationFilterState[NUMBER_OF_DATA_LINES*PDM_TI_DECIMATION_FILTER_STATE_SIZE]={0};
#endif

PDM_Transaction pdmTransaction1;
PDM_Transaction pdmTransaction2;
PDM_Transaction pdmTransaction3;
PDM_Transaction pdmTransaction4;
PDM_Transaction pdmTransaction5;
PDM_Transaction pdmTransaction6;

List_List pdmList;

static PDM_Transaction *pdmTransactionList[NUMBER_OF_TRANSACTIONS] = {&pdmTransaction1, &pdmTransaction2, &pdmTransaction3, &pdmTransaction4, &pdmTransaction5, &pdmTransaction6};

static int8_t* pdmBufList[NUMBER_OF_TRANSACTIONS] = {pdmBuf1, pdmBuf2, pdmBuf3, pdmBuf4, pdmBuf5, pdmBuf6};

/*
 * Application parameters
 */
PDM_decimationFilter myDecimationFilter;
PDM_Params pdmParams;


/*
 * Application PDM events
 */
//Event_Struct sPDMEvents;
//Event_Handle pdmEvents;

/*
#define PDM_EVT_START                   Event_Id_01
#define PDM_EVT_STOP                    Event_Id_02
#define PDM_EVT_WAIT_DECIMATION         Event_Id_03
#define PDM_EVT_DATA_READY              Event_Id_04
*/
/*
 * Static functions and variables
 */
static void pdmCallbackFxn(PDM_Handle pdmHandle, int_fast16_t status, PDM_Transaction *transactionPtr);
void gpioButtonFxn0(uint_least8_t index);
void gpioButtonFxn1(uint_least8_t index);

/*
 *  ======== gpioButtonFxn0 ========
 *  Callback function for the GPIO interrupt on Board_GPIO_BUTTON0.
 */
void gpioButtonFxn0(uint_least8_t index){

    if(!transferIsRunning) {
        Event_post(pdmEvents, PDM_EVT_START);
        transferIsRunning = true;
    }
}

/*
 *  ======== gpioButtonFxn1 ========
 *  Callback function for the GPIO interrupt on Board_GPIO_BUTTON1.
 */
void gpioButtonFxn1(uint_least8_t index){

    if(transferIsRunning){
        Event_post(pdmEvents, PDM_EVT_STOP);
        transferIsRunning = false;
    }
}

/*
 *  ======== pdmCallbackFxn ========
 */
static void pdmCallbackFxn(PDM_Handle pdmHandle, int_fast16_t status, PDM_Transaction *transactionPtr) {

    if(status & PDM_TRANSACTION_SUCCESS) {

        Event_post(pdmEvents, PDM_EVT_WAIT_DECIMATION);
    }

    if(status & PDM_UNDERFLOW) {
        Event_post(pdmEvents, PDM_EVT_STOP);
        transferIsRunning = false;
        /* If needed turn off microphone power here */
    }

    if(status & PDM_I2S_ERROR){
        Event_post(pdmEvents, PDM_EVT_STOP);
        transferIsRunning = false;
        /* If needed turn off microphone power here */
    }
}

/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    /*
     * Drivers initialization
     */
    GPIO_init();
    //UART_init();
    PDM_init();
    //printf("Welcome PDM \n");
    /* Semaphore and event for the task */
    Event_Params eventParams;
    uint32_t events = 0;
    Event_Params_init(&eventParams);
    Event_construct(&sPDMEvents, &eventParams);
    pdmEvents = Event_handle(&sPDMEvents);

   Event_Params birdeventParams;
   //uint32_t birdevents = 0;
   Event_Params_init(&birdeventParams);
   Event_construct(&sbirdEvents, &birdeventParams);
   birdEvents = Event_handle(&sbirdEvents);


    /*
     * LEDs and Buttons pins opening
     */
    /* Configure the LEDs and buttons pins */
    GPIO_setConfig(Board_GPIO_LED0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(Board_GPIO_LED1, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(Board_GPIO_BUTTON0, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);
    GPIO_setConfig(Board_GPIO_BUTTON1, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);

    /* install Button's callback */
    GPIO_setCallback(Board_GPIO_BUTTON0, gpioButtonFxn0);
    GPIO_setCallback(Board_GPIO_BUTTON1, gpioButtonFxn1);

    /* Enable interrupts */
    GPIO_enableInt(Board_GPIO_BUTTON0);
    GPIO_enableInt(Board_GPIO_BUTTON1);

    /*
     * UART opening
     */
    /*UART_Handle uartHandle;
    UART_Params uartParams;

    UART_Params_init(&uartParams);
    uartParams.writeDataMode = UART_DATA_TEXT;
    uartParams.baudRate = UART_BAUD_RATE;
    uartHandle = UART_open(Board_UART0, &uartParams);
    */
    while(1){

        events = Event_pend(pdmEvents, Event_Id_NONE, (PDM_EVT_START|PDM_EVT_STOP|PDM_EVT_WAIT_DECIMATION), BIOS_WAIT_FOREVER);
        //printf("PDM thread \n");

        if (events & PDM_EVT_START) {

            #ifdef USE_CUSTOM_FILTER
            myDecimationFilter = PDMCustomFilter_decimation120;
            myDecimationFilter.pcmBufferSize         = PCM_BUF_SIZE;
            myDecimationFilter.pdmBufferSize         = PDM_BUF_SIZE;
            #else
            myDecimationFilter = getTIDecimationFilter(DECIMATION_FACTOR,NUMBER_OF_DATA_LINES);
            myDecimationFilter.pcmBufferSize         = PCM_BUF_SIZE;
            myDecimationFilter.pdmBufferSize         = PDM_BUF_SIZE;
            myDecimationFilter.decimationFilterStateMemory = decimationFilterState;
            #endif

            PDM_Params_init(&pdmParams);
            pdmParams.activatedLines        = (1 << NUMBER_OF_DATA_LINES) - 1;
            pdmParams.pdmFrequency          = PDM_FREQUENCY;
            pdmParams.clockSource           = PDM_INTERNAL_CLOCK_SOURCE;
            pdmParams.userCallback          = pdmCallbackFxn;
            pdmParams.filter                = &myDecimationFilter;

            microphonePdmHandle = PDM_open(Board_PDM0, &pdmParams);

            /* Initialize and queue the PDM transactions */
            uint8_t k;
            for(k = 0; k < NUMBER_OF_TRANSACTIONS; k++) {

                /* Initialization of the PDM transactions */
                PDM_Transaction_init(pdmTransactionList[k]);
                pdmTransactionList[k]->bufPtr  = pdmBufList[k];
                pdmTransactionList[k]->bufSize = PDM_BUF_SIZE;
                pdmTransactionList[k]->arg     = (uintptr_t)microphonePdmHandle;
                List_put(&pdmList, (List_Elem*)pdmTransactionList[k]);
            }

            List_tail(&pdmList)->next = List_head(&pdmList); /* PDM buffers are queued in a ring-list */
            List_head(&pdmList)->prev = List_tail(&pdmList);

            PDM_setQueueHead(microphonePdmHandle, &pdmTransaction1);

            PDM_startClock(microphonePdmHandle);
            PDM_startStream(microphonePdmHandle);

            GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_ON);
        }

        if (events & PDM_EVT_STOP) {

            PDM_stopStream(microphonePdmHandle);
            PDM_stopClock(microphonePdmHandle);
            PDM_close(microphonePdmHandle);

            GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_OFF);
        }

        if (events & PDM_EVT_WAIT_DECIMATION) {

            bool pcmSuccess;
            PDM_Transaction *pdmTransactionUsed;

            pcmSuccess = PDM_getPCMData(microphonePdmHandle, pcmBufL1, NULL, &pdmTransactionUsed);

            if(pcmSuccess == true){
                Event_post(birdEvents, PDM_EVT_DATA_READY);
                GPIO_toggle(Board_GPIO_LED1);
            }
            else
            {
                GPIO_toggle(Board_GPIO_LED0);
            }
        }

        if(events & PDM_EVT_DATA_READY) {
            /*int i;
            #ifndef SEND_ADPCM_DATA
            uartBuff[0] = sequenceNumber;
            memcpy(&uartBuff[4], pcmBufL1, sizeof(pcmBufL1));
            for(i = 0; i < PCM_BUF_SIZE; i++)
           {
               printf("%f \n", (float)pcmBufL1[i]/32000);
           }
            /*UART_write(uartHandle, uartBuff, sizeof(uartBuff));
            #else
            uint8_t dataLength = Codec1_encodeBuff(&adpcmBuf[4], pcmBufL1, PCM_BUF_SIZE, &si, &pv);
            adpcmBuf[0] = sequenceNumber;
            adpcmBuf[1] = si;
            adpcmBuf[2] = (pv & 0x00FF);
            adpcmBuf[3] = ((pv & 0xFF00) >> 8);
            UART_write(uartHandle, adpcmBuf, dataLength+UART_HEADER_SIZE);
            #endif

            sequenceNumber++;*/
        }
    }
}
