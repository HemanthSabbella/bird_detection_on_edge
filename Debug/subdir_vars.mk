################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../CC1352R1_LAUNCHXL_TIRTOS.cmd 

C_SRCS += \
../CAR.c \
../CC1352R1_LAUNCHXL.c \
../CC1352R1_LAUNCHXL_fxns.c \
../SVM_inference.c \
../bm_ihc.c \
../ccfg.c \
../main_file.c \
../main_tirtos.c \
../normalized.c \
../pdmstream.c \
../sample_thread.c 

C_DEPS += \
./CAR.d \
./CC1352R1_LAUNCHXL.d \
./CC1352R1_LAUNCHXL_fxns.d \
./SVM_inference.d \
./bm_ihc.d \
./ccfg.d \
./main_file.d \
./main_tirtos.d \
./normalized.d \
./pdmstream.d \
./sample_thread.d 

OBJS += \
./CAR.obj \
./CC1352R1_LAUNCHXL.obj \
./CC1352R1_LAUNCHXL_fxns.obj \
./SVM_inference.obj \
./bm_ihc.obj \
./ccfg.obj \
./main_file.obj \
./main_tirtos.obj \
./normalized.obj \
./pdmstream.obj \
./sample_thread.obj 

OBJS__QUOTED += \
"CAR.obj" \
"CC1352R1_LAUNCHXL.obj" \
"CC1352R1_LAUNCHXL_fxns.obj" \
"SVM_inference.obj" \
"bm_ihc.obj" \
"ccfg.obj" \
"main_file.obj" \
"main_tirtos.obj" \
"normalized.obj" \
"pdmstream.obj" \
"sample_thread.obj" 

C_DEPS__QUOTED += \
"CAR.d" \
"CC1352R1_LAUNCHXL.d" \
"CC1352R1_LAUNCHXL_fxns.d" \
"SVM_inference.d" \
"bm_ihc.d" \
"ccfg.d" \
"main_file.d" \
"main_tirtos.d" \
"normalized.d" \
"pdmstream.d" \
"sample_thread.d" 

C_SRCS__QUOTED += \
"../CAR.c" \
"../CC1352R1_LAUNCHXL.c" \
"../CC1352R1_LAUNCHXL_fxns.c" \
"../SVM_inference.c" \
"../bm_ihc.c" \
"../ccfg.c" \
"../main_file.c" \
"../main_tirtos.c" \
"../normalized.c" \
"../pdmstream.c" \
"../sample_thread.c" 


