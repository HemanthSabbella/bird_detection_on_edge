/*ULTRA_LIGHT AUDIO CLASSIFIER*/


#include<math.h>
#include<stdio.h>
#include<string.h>
#include <unistd.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/BIOS.h>
#include "pdmstream.h"
#include "Board.h"
//#include"./audio_headers/neg1_koala.h"
//#include"./audio_headers/neg2_koala.h"
//#include"./audio_headers/neg3_kangaroo.h"
//#include"./audio_headers/neg4_kangaroo.h"
//#include"./audio_headers/neg5_fox.h"
//#include"./audio_headers/neg6_fox.h"
//#include"./audio_headers/pos1.h"
//#include"./audio_headers/pos2.h"
//#include"./audio_headers/pos3.h"
//#include"./audio_headers/pos4.h"
//#include"./audio_headers/pos5.h"
//#include"audiofile.h"
//#include"audiofile.h"
#include"alpha.h"
#include"bias.h"
#include"CAR.h"
#include"normalized.h"
#include"SVM_inference.h"
#include"bm_ihc.h"

#define NSAMPLES 16000
struct filter_coeff filter[NFILT];
float w0[NFILT],w1[NFILT],IHC_sum[NFILT];
float kernel[NEND-NSTART+1];
float ihcSum[NEND-NSTART+1];
int result;
int sample_cnt = 0;
float sample = 0;
uint16_t audioval[PCM_BUF_SIZE];
uint32_t bird_event;
void *bird_detection()
{	
	int i,j;
	float x_l=X_LOW;
	float x_h=X_HIGH;
	unsigned int nfilt=NFILT;
	unsigned int fsampling=FS, n_kernels=NEND-NSTART+1;	
	filterCoeff(filter, x_l , x_h,fsampling, nfilt);
	int filt_n=0;
	//printf("Welcome!! \n");

	while(1){

	bird_event = Event_pend(birdEvents, Event_Id_NONE, PDM_EVT_DATA_READY, BIOS_WAIT_FOREVER);

	//printf("sample_cnt %d \n", sample_cnt);
	//if (bird_event & PDM_EVT_DATA_READY)
	//{
	    //audioval[0] = 0;
        //for(j=1;j<NSAMPLES;j++)
	    memcpy(&audioval, pcmBufL1, sizeof(pcmBufL1));
	    //printf("audioval");

	    for(j=0; j<PCM_BUF_SIZE; j++)
        {
            //sample=audioval[j];
	        sample = (float)(audioval[j]/32000);
	        //GPIO_toggle(Board_GPIO_LED0);

	        bm_ihc(filter,w0,w1,sample,IHC_sum,nfilt);

            //printf("%d\n", sample_cnt);
            if (sample_cnt == FS-2)
            {
                //printf("Processing..... \n");
                filt_n=0;
                for(i=0;i<NFILT;i++)
                {
                    if( i >= NSTART-1 && i < NEND)
                    {
                        ihcSum[filt_n]=IHC_sum[i];

                        //printf("SUM[%d] = %6f \n",filt_n,ihcSum[filt_n]);
                        filt_n++;
                    }
                }

                normalize(&ihcSum[0], n_kernels , 0 , 1);
                int result = SVM_Inference( n_kernels,&ihcSum[0],alpha,bias);
                GPIO_toggle(Board_GPIO_LED0);

                //printf("result %d \n");
                //int class = (result==0)?1:0;
                //printf("\nDetected Class = %d \t with sample nu= %d\n", class,j);
//                memset((void*)(&w0[0]),0,nfilt);
//                memset((void*)(&w1[0]),0,nfilt);
//                memset((void*)(&IHC_sum[0]),0,nfilt);
                sample_cnt=0;
                //return 0;
            }
            else
            {
                sample_cnt++;
            }
            //Task_sleep(5 * (100 / Clock_tickPeriod));
        }
	   // }

	}
	return 0;
}
