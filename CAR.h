#ifndef _CAR_H_
#define _CAR_H_

#define NFILT 35
#define NSTART 6
#define NEND 35
#define X_LOW 0.2139
#define X_HIGH 0.6104
#define FS 16000


struct filter_coeff
{
	float rf;
	float a0f;
	float c0f;
	float gh;
	float hf;
};
void filterCoeff(struct filter_coeff coeffPtr[NFILT],float x_low, float x_high,unsigned int fs,unsigned int totalFilters);

#endif
